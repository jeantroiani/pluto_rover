# plutoKata
Kata of rover on pluto.

type "karma start"
From the terminal to run the test suite.

Tech used:
JavaScript, Jasmine and Karma.

Exist three classes:

Rover:
Represents the vehicle, it can be instantiated with X, Y and orientation. If not, it gets 0 , 0 and N values by default.
Its Orientation system works only in 90 degrees, N, E, S and W.

World:
The other class is World or map, instance gets created with width and height.
It has support to deploy items on it and clear them too.

Controller:
The Controller works with both classes previously mentioned, and coordinates the commands given by the user.
The current rules for commands and how the vehicle interacts with the grid are written in this class.

