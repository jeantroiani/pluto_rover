"use strict";

class Controller {

    constructor  (vehicle, grid) {
        if(vehicle && grid){
            this.vehicle = vehicle;
            this.grid = grid;
            this.deployVehicle();
        } else {
            throw new Error(`Controller to be instantitate with vehicle and grid`);
        }
    }

    deployVehicle () {
        this.grid.createObstacle(this.vehicle.currentPosition()[0],this.vehicle.currentPosition()[1]);
    }

    arraySanitize (array) {
        array.forEach( (command) => {
            if ( !this.vehicle[command] ) {
                throw new Error(`Command ${command} not available`)
            }
        });
        return array;
    }

    stringParser (string) {
        const arrayCommands =  string.split("");
        try {
            if (this.arraySanitize(arrayCommands)) { return arrayCommands; }
        }
        catch (e) {
            return e.message;
        }
    }

    move (string) {
        if (this.stringParser(string)) { var stringCommands = this.stringParser(string) }
        stringCommands.forEach((command) => {
            const oldPosition = this.vehicle.currentPosition();
            const oldOrientation = this.vehicle.currentOrientation();
            this.vehicle[command]();
            const newPosition = this.vehicle.currentPosition();
            const newOrientation = this.vehicle.currentOrientation();

            //blocked
            if (this.grid.isPositionInGrid(newPosition[0], newPosition[1]) && this.grid.isPositionBlocked(newPosition[0], newPosition[1] )) {
                this.vehicle.setCurrentPosition(oldPosition[0], oldPosition[1]);
                this.grid.createObstacle(this.vehicle.currentPosition()[0],this.vehicle.currentPosition()[1]);
                return;
            }
            //Not going out of grid
            if (!this.grid.isPositionGridLimit(oldPosition[0], oldPosition[1]) && this.grid.isPositionInGrid(newPosition[0], newPosition[1])) {
                this.grid.createObstacle(this.vehicle.currentPosition()[0],this.vehicle.currentPosition()[1]);
                this.grid.removeObstacle(oldPosition[0], oldPosition[1]);
            //going out of grid
            } else {
                if (oldPosition[1] === 0 && oldOrientation === "S") {
                    this.grid.createObstacle(oldPosition[0], this.grid.height - 1);
                    this.grid.removeObstacle(oldPosition[0], oldPosition[1]);
                    this.vehicle.setCurrentPosition(oldPosition[0], this.grid.height - 1);
                }

                if (oldPosition[1] === this.grid.height - 1 && oldOrientation === "N") {
                    this.grid.createObstacle(oldPosition[0], 0);
                    this.grid.removeObstacle(oldPosition[0], oldPosition[1]);
                    this.vehicle.setCurrentPosition(oldPosition[0], 0);
                }

                if (oldPosition[0] === this.grid.width - 1 && oldOrientation === "E") {
                    this.grid.createObstacle(0, oldPosition[1]);
                    this.grid.removeObstacle(oldPosition[0], oldPosition[1]);
                    this.vehicle.setCurrentPosition( 0, oldPosition[1]);
                }

                if (oldPosition[0] === 0 && oldOrientation === "W") {
                    this.grid.createObstacle(this.grid.width - 1, oldPosition[1]);
                    this.grid.removeObstacle(oldPosition[0], oldPosition[1]);
                    this.vehicle.setCurrentPosition( this.grid.width - 1, oldPosition[1]);
                }
            }
        });
    }
}
