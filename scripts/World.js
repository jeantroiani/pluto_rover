"use strict";

class World {
    constructor (width, height) {
        if (width ||height) {
            this.width = width;
            this.height = height;
            this.grid = [];
            this.build();
        } else {
            throw new Error (`Must be initialized with width and height`);
        }
    }

    build () {
        this.grid.push(new Array(this.height));
        for(let y = 0; y < this.height; y++){
            for(let x = 0; x < this.width; x++) {
                this.grid[y] = new Array(this.width)
            }
        }
    }

    createObstacle (positionX, positionY) {
      if (this.isPositionInGrid) {
        this.grid[positionY][positionX] = {obstacle:true}
      } else {
        throw new Error(`Error: ${isPositionInGrid} not in the grid.`)
      }
    }

    removeObstacle (positionX, positionY) {
      if (this.isPositionInGrid) {
        this.grid[positionY][positionX] = {obstacle:false}
      } else {
        throw new Error(`Error: ${isPositionInGrid} not in the grid.`)
      }
    }

    isPositionInGrid (positionX, positionY) {
        return ((this.width > positionX && positionY > 0) && (this.height > positionY && positionY > 0)) ? true : false;
    }

    isPositionGridLimit (positionX, positionY) {
        return (positionY === 0 || positionY === this.height  - 1 || positionX === 0 || positionX === this.height  - 1) ? true : false;
    }

    isPositionBlocked (positionX, positionY) {
        if(this.isPositionInGrid(positionX, positionY)) {
            return (this.grid[positionY][positionX] === undefined || this.grid[positionY][positionX].obstacle === false ) ? false : true;
        } else {
            return false;
        };
    }

}
