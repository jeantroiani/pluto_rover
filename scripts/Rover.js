"use strict";

class Rover {

    constructor (initialPosX, initialPosY, initOrientation) {
        this.positionX = initialPosX || 0;
        this.positionY = initialPosY || 0;
        this.orientation = initOrientation || "N";
    }

    currentPosition () {
        return ([this.positionX, this.positionY]);
    }

    currentOrientation () {
        return (this.orientation);
    }

    setCurrentPosition (positionX, positionY) {
        return ([this.positionX = positionX], [this.positionY = positionY]);
    }

    orientationSystem () {
        return {
            "N": "E",
            "E": "S",
            "S": "W",
            "W": "N"
        }
    }

    F () {
        this.move("forward");
    }

    B () {
        this.move("backwards");
    }

    R () {
        this.orientation = this.orientationSystem()[this.orientation];
    }

    L () {
        this.orientation = this.inverseObj(this.orientationSystem())[this.orientation];
    }

    inverseObj (obj) {
        let inverted = {};
	    for (let x of Object.keys(obj)){
	      inverted[obj[x]] = x;
	    }
    	return inverted;
    }

    move (direction) {
        switch (this.orientation) {
            case "N" :
                direction == "forward" ? this.positionY += 1 : this.positionY -= 1;
                break;
            case "S" :
                direction == "forward" ? this.positionY -= 1 : this.positionY += 1;
                break;
            case "E" :
                direction == "forward" ? this.positionX += 1 : this.positionX -= 1;
                break;
            case "W" :
                direction == "forward" ? this.positionX -= 1 : this.positionX += 1;
                break;
        };
    }

}
