"use strict";

describe ("Controller", function () {

    it ("is instantiated with vehicle and map", function () {
        const pluto = new World(2, 2);
        const rover = new Rover();
        const controller = new Controller(rover, pluto);

        expect(typeof controller == "object").toEqual(true);
    });

    it ("throws an error if created without vehicle and map", function () {
        expect(function () { new Controller(); }.bind(this)).toThrow(new Error("Controller to be instantitate with vehicle and grid"));
    });

    it ("places a vehicle in the current vehicle position", function () {
        const pluto = new World(2, 2);
        const rover = new Rover();
        const controller = new Controller(rover, pluto);

        expect(pluto.grid[0][0]).toEqual({obstacle: true});
    });

    describe("string parser", function () {

        const pluto = new World(2, 2);
        const rover = new Rover();
        const controller = new Controller(rover, pluto);
        const commandString = "FR";
        const invalidCommandString = "FZ";

        it ("receives a string and converts it to array of commands", function () {
            expect(controller.stringParser(commandString)).toEqual(["F","R"]);
        });

        it ("rejects string of commands if at least one is not available", function () {
            expect(controller.stringParser(invalidCommandString)).toEqual("Command Z not available");
        });

    });

    describe("move", function () {

        it ("gives a parsed string of actions to the vehicle", function () {
            const rover = new Rover(1, 1, "N");
            const pluto = new World(4, 4);
            const controller = new Controller(rover, pluto);
            const commandString = "FRF";

            controller.move(commandString);
            expect(rover.currentPosition()).toEqual([2, 2]);
            expect(rover.currentOrientation()).toEqual("E");
        })

        it ("changes position of vehicle on grid", function () {
            const rover = new Rover(1, 1, "N");
            const pluto = new World(4, 4);
            const controller = new Controller(rover, pluto);
            const commandString = "F";

            controller.move(commandString);
            expect(pluto.grid[2][1]).toEqual({obstacle: true});
        })

        it ("clears previous position of vehicle", function () {
            const rover = new Rover(1, 1, "N");
            const pluto = new World(4, 4);
            const controller = new Controller(rover, pluto);
            const commandString = "F";

            controller.move(commandString);
            expect(pluto.grid[1][1]).toEqual({obstacle: false});
        })

        describe ("wraps actions from edge to edge, vehicle on", function () {

            it ("bottom of grid facing South move forward goes to the top of the grid", function () {
                const rover = new Rover(1, 0, "S");
                const pluto = new World(2, 2);
                const controller = new Controller(rover, pluto);
                const commandString = "F";

                controller.move(commandString);
                expect(pluto.grid[1][1]).toEqual({obstacle: true});
                expect(pluto.grid[0][1]).toEqual({obstacle: false});
                expect(rover.currentPosition()).toEqual([1, 1]);
            })

            it ("top of grid facing North move forward goes to the bottom of the grid", function () {
                const rover = new Rover(1, 1, "N");
                const pluto = new World(2, 2);
                const controller = new Controller(rover, pluto);
                const commandString = "F";

                controller.move(commandString);
                expect(pluto.grid[0][1]).toEqual({obstacle: true});
                expect(pluto.grid[1][1]).toEqual({obstacle: false});
                expect(rover.currentPosition()).toEqual([1, 0]);
            })

            it ("far right vehicle facing East move forward goes to the far left of the grid", function () {
                const rover = new Rover(1, 0, "E");
                const pluto = new World(2, 2);
                const controller = new Controller(rover, pluto);
                const commandString = "F";

                controller.move(commandString);
                expect(pluto.grid[0][0]).toEqual({obstacle: true});
                expect(pluto.grid[0][1]).toEqual({obstacle: false});
                expect(rover.currentPosition()).toEqual([0, 0]);
            })

            it ("far left vehicle facing West move forward goes to the far right of the grid", function () {
                const rover = new Rover(0, 1, "W");
                const pluto = new World(2, 2);
                const controller = new Controller(rover, pluto);
                const commandString = "F";

                controller.move(commandString);
                expect(pluto.grid[1][1]).toEqual({obstacle: true});
                expect(pluto.grid[1][0]).toEqual({obstacle: false});
                expect(rover.currentPosition()).toEqual([1, 1]);
            })

        });

        describe ("on obstacle", function () {

            it ("stops vehicle and reports last position", function () {
                const rover = new Rover(0, 0, "N");
                const pluto = new World(3, 3);
                pluto.createObstacle(1, 1);
                const controller = new Controller(rover, pluto);
                const commandString = "FRF";

                controller.move(commandString);
                expect(rover.currentOrientation()).toEqual("E");
                expect(rover.currentPosition()).toEqual([0, 1]);
                expect(pluto.grid[1][0]).toEqual({obstacle: true});
            })
        });
    });
});
