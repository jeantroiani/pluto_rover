"use strict";

describe ("World", function () {

    it ("can be instantiated", function () {
        const pluto = new World(1, 2);

        expect(typeof pluto == "object").toEqual(true);
    });

    describe ("when creating it", function () {

        it ("accepts argument for width", function(){
            const pluto = new World(1, 2);

            expect(pluto.width).toEqual(1);
        });

        it ("accepts argument for height", function(){
            const pluto = new World(1, 2);

            expect(pluto.height).toEqual(2);
        });
    });

    describe ("grid", function () {

        it ("is an array with its height and width", function(){
            const pluto = new World(4, 3);

            expect(typeof pluto.grid == "object").toEqual(true);
        });

        it ("returns true if a position lies inside in the grid", function(){
            const pluto = new World(4, 4);

            expect(pluto.isPositionInGrid(2, 3)).toEqual(true);
        });

        it ("returns false if a position is not in the grid", function(){
            const pluto = new World(4, 4);

            expect(pluto.isPositionInGrid(4, 4)).toEqual(false);
        });

        it ("accepts obstacles", function(){
            const pluto = new World(4, 4);
            pluto.createObstacle(2,2);
            expect(pluto.grid[2][2]).toEqual({obstacle: true});
        });

        it ("clears obstacles on specific position", function(){
            const pluto = new World(4, 4);
            pluto.createObstacle(2,2);
            pluto.removeObstacle(2,2);
            expect(pluto.grid[2][2]).toEqual({obstacle: false});
        });

        it("returns true if a position is in the top or bottom border of the grid", function () {
            const pluto = new World(4, 4);
            expect(pluto.isPositionGridLimit(2,0)).toEqual(true);
        });

        it("returns true if a position is in the far left or far right border of the grid", function () {
            const pluto = new World(4, 4);
            expect(pluto.isPositionGridLimit(0, 2)).toEqual(true);
        });

        it("returns false if a position is not on any of the four borders", function () {
            const pluto = new World(4, 4);
            expect(pluto.isPositionGridLimit(1, 1)).toEqual(false);
        });

        it("returns true if a position has a blockage", function () {
            const pluto = new World(4, 4);
            pluto.createObstacle(2, 3);
            expect(pluto.isPositionBlocked(2, 3)).toEqual(true);
        });

        it("returns false if a position is empty", function () {
            const pluto = new World(4, 4);
            pluto.createObstacle(2, 3);
            expect(pluto.isPositionBlocked(1, 3)).toEqual(false);
        });

    });

});
