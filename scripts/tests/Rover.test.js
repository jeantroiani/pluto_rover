"use strict";
describe ("A Rover", function () {

    it ("can be instantiated", function () {
        const rover = new Rover();
        expect(typeof rover == "object").toEqual(true);
    });

    it ("can be initialize with an especific position, and the attribute can be readed", function () {
        const rover = new Rover(1, 1, "N");
        expect(rover.currentPosition()).toEqual([1, 1]);
    });

    it ("can be initialize with an especific orientation, and the attribute can be readed", function () {
        const rover = new Rover(1, 1, "N");
        expect(rover.currentOrientation()).toEqual("N");
    });

    it ("can be initialize without giving position, it gets created with a default coordinate at 0 0", function () {
        const rover = new Rover();
        expect(rover.currentPosition()).toEqual([0, 0]);
    });

    it ("can be initialize without giving orientation, it gets created with a default orientation North", function () {
        const rover = new Rover();
        expect(rover.currentOrientation()).toEqual("N");
    });

    it ("orientation and position can be reset to a position", function () {
        const rover = new Rover(1, 2);
        rover.setCurrentPosition(3, 4)
        expect(rover.currentPosition()).toEqual([3, 4]);
    });

    describe ("accepts command", function () {

        it ("move forward and moves 1 space to the front", function () {
            const rover = new Rover(1, 1, "N");
            rover.F();
            expect(rover.currentPosition()).toEqual([1, 2]);
            expect(rover.currentOrientation()).toEqual("N");
        });

        it ("move forward, based on current orientation", function () {
            const rover = new Rover(1, 1, "W");
            rover.F();
            expect(rover.currentPosition()).toEqual([0, 1]);
            expect(rover.currentOrientation()).toEqual("W");
        })

        it ("move back and moves 1 space backwards", function () {
            const rover = new Rover(1, 1, "S");
            rover.B();
            expect(rover.currentPosition()).toEqual([1, 2]);
            expect(rover.currentOrientation()).toEqual("S");
        });

        it ("move back, based on current orientation", function () {
            const rover = new Rover(1, 1, "E");
            rover.B();
            expect(rover.currentPosition()).toEqual([0, 1]);
            expect(rover.currentOrientation()).toEqual("E");
        })

        describe("rotate", function () {

            it ("rotate right, if oriented north should update to east", function () {
                const rover = new Rover(1, 1, "N");
                rover.R();
                expect(rover.currentOrientation()).toEqual("E");
            })

            it ("rotate right, if oriented west should update to nort", function () {
                const rover = new Rover(1, 1, "W");
                rover.R();
                expect(rover.currentOrientation()).toEqual("N");
            })

            it ("rotate left, if oriented east should update to north", function () {
                const rover = new Rover(1, 1, "E");
                rover.L();
                expect(rover.currentOrientation()).toEqual("N");
            })

            it ("rotate left, if oriented west should update to south", function () {
                const rover = new Rover(1, 1, "W");
                rover.L();
                expect(rover.currentOrientation()).toEqual("S");
            });
            
        });

    });

});
